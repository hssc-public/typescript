const number:number = 25;
const str: string = 'hello';

const obj:{name: string} = {
    name: 'aa'
}

const arr: string[] = []

const tuple: [string, number, {name: string}, boolean] = ['str', 25, {name: 'string'}, true];

enum Color{
    'RED',
    'BLUE',
    'GREEN'
}

const blue = Color.BLUE;


