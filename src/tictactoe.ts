import { Board as GameBoard, Mark} from './board';
import { Player } from './player';

export class TicTacToe{
    board: GameBoard;
    currentPlayer: Player;
    players: Player[] = [];
    constructor (player1Name: string = 'Player1', player2Name: string = 'Player2'){
        this.board = new GameBoard();
        this.players = [];
        this.players.push(new Player(player1Name, Mark.X));
        this.players.push(new Player(player2Name, Mark.O));
        this.currentPlayer = this.players[0];
    }
    placeMark(x:number, y:number): string {
        if (this.board.fieldEmpty(x, y)) {
            this.board.place(x, y, this.currentPlayer.mark);
            this.changePlayer();
        }
        let winner = this.whoWins();
        if(winner){
            return 'Return the winner is ' + winner;
        } else {
            //check for tie
            return 'The next player is' + this.currentPlayer;
        }
    }
    private changePlayer():void {
        this.currentPlayer = this.currentPlayer.mark === Mark.X ? this.players[1] : this.players[0];
    }
    private whoWins(): Player | void {
        for (let triplets of this.board) {
            const result = triplets.data
                .filter((value: Mark | null): boolean => !!value)
                .reduce((accumulator: {X: number, O: number}, value: Mark | null): {X: number, O: number}  =>{
                    if(value === Mark.X){
                        accumulator.X++
                    } else if(value === Mark.O){
                        accumulator.O++;
                    }
                    return accumulator;
                }, {X: 0, O:0});
                console.log('whoWins', triplets.data, result);
            if(result.X === 3){
                return this.players.find((player) => player.mark === Mark.X);
            } else if(result.O === 3){
                return this.players.find((player) => player.mark === Mark.O);;
            }
        }
    }

}

function log(target: any, propertyKey: string, descriptor: PropertyDescriptor){
    console.log('LOG: ', target);
    console.log('LOG: ', propertyKey);
    console.log('LOG: ', descriptor);
}

function logFactory(message: string){
    return (target: any, propertyKey: string, descriptor: PropertyDescriptor) => {
        console.log(message, target);
        console.log(message, propertyKey);
        console.log(message, descriptor);
    }
}