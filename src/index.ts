import { TicTacToe } from "./tictactoe";

const game = new TicTacToe();
console.log(typeof game);
console.log(game.board instanceof Array);
console.log(Array.isArray(game.board));

try {
    game.placeMark(1, 1);
    game.placeMark(0, 2);
    game.placeMark(1, 2);
    console.log(game.placeMark(0, 1));;
    console.log(game.placeMark(1, 0));
} catch (e) {
    console.log('Error ', e.message);
}
finally {
    console.log('Finally');
    console.log(game.board);
}

for (let pos of game.board) {
    console.log('iterator', pos);
}
