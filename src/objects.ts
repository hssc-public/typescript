const person: {name: string, age: number} = {
    name: 'Adam',
    age:42
}
// let age = person.age;
let {age} = person;

console.log(age);