function add(x: string, y:string, z?: string): number;
function add(x: number, y:number, z?:number): number;
function add(x: any, y: any, z?:any): number {
    if(typeof x === 'string'){
        return parseFloat(x) + parseFloat(y) + parseFloat(z);
    }
    return x + y;
}

const addArrow: (x: number, y: number) => number =  (x, y) => x+y;

const objectWithFunction:{ name: string, getFullName: ()=> string, setName: (newName: string) => void} = {
    name: 'Adam',
    getFullName(){
        return this.name;
    },
    setName(newName){
        this.name = newName;
    }
}
