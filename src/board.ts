export enum Mark{
    'X'=1,
    'O'
}

type MarkType = Mark | null
export class Board{
    private fields: Array<MarkType[]>;
    constructor() {
        this.fields = [
            [null, null, null],
            [null, null, null],
            [null, null, null]
        ];
    }

    public place(x:number , y:number, mark: Mark):void {
        checkBound(x, 'X', 0, 2);
        checkBound(y, 'Y', 0, 2);
        if (!this.fields[x][y]) {
            this.fields[x][y] = mark;
        }
    }

    public fieldEmpty(x: number, y: number):boolean {
        return !this.fields[x][y] ? true : false;
    }

    private * getRows() {
        for (let row of this.fields) {
            yield {data: row, type: 'row'};
        }
    }

    private * getColumns() {
        for (let i = 0; i < this.fields.length; i++) {
            yield {
                data: [
                    this.fields[0][i],
                    this.fields[1][i],
                    this.fields[2][i]
                ],
                type: 'column'
            };
        }
    }

    private * getPossibilities() {
        yield* this.getRows();
        yield* this.getColumns();
    }

    [Symbol.iterator]() {
        return this.getPossibilities();
    }

}

function checkBound(value: number, name: string, min: number, max: number) {
    if (value < min || value > max) {
        throw new Error(`${name} must be positive number and not greater than ${max}`);
    }
}
