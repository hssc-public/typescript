enum CardColor{
    'HEARTS',
    'CLUBS',
    'SPADES',
    'DIAMONDS'
}

interface ICard {
    readonly value: string;
    color: CardColor;
}

const card: ICard = {
    value: '10',
    color: CardColor.HEARTS
}

class Card implements ICard{
    private _value: string;
    private _color: CardColor;

    constructor(val: string, color: CardColor){
        this._value = val;
        this._color = color;
    }

    get value(){
        return this._value;
    }

    get color(){
        return this._color;
    }

    set color(color: CardColor){
        this._color = color;
    }

}

const card2: ICard = new Card('8', CardColor.CLUBS);